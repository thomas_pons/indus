'use strict';

angular.module('valphoneApp')
    .directive('ngDroppable', ["$rootScope", function($rootScope){

        function dragEnter(evt, element, dropStyle){
            evt.preventDefault();
            element.addClass(dropStyle);
        };

        function dragLeave(evt, element, dropStyle){
            element.removeClass(dropStyle);
        };

        function dragOver(evt, element, dropStyle){
            evt.preventDefault();
            element.addClass(dropStyle);
        };

        function drop(evt, element, dropStyle){
            evt.preventDefault();

            var files = evt.dataTransfer.files;

            if(files.length == 1){
                if(files[0].type.match('image/jpeg')){

                    var img = document.createElement('img');
                    var reader = new FileReader();
                    reader.readAsDataURL(files[0]);

                    reader.onloadend = function(e){
                        img.src = e.target.result;
                    }

                    element.html("");
                    element.children().remove();
                    element.append(img);

                }else{
                    toastr.error("Invalid Mime/type. Only JPG file authorized.");
                }

            }else if(files.length > 1){
                toastr.error("You can drop only one JPG file here.");
            }else{

                var draggedElement = angular.element(document.getElementById(evt.dataTransfer.getData("id")));

                if(draggedElement.attr('draggable')){
                    element.html("");
                    element.children().remove();
                    draggedElement.removeAttr('draggable');
                    element.append(draggedElement);

                }else{
                    toastr.error("This element is not draggable or has been already drop.");
                }
            }

            element.removeClass(dropStyle);
        };

        return {
            restrict: 'A',
            link: function(scope, element, attrs){
                scope.dropStyle = attrs["ngDropstyle"];
                element.bind('dragenter', function(evt){
                    dragEnter(evt, element, scope.dropStyle);
                });
                element.bind('dragleave', function(evt){
                    dragLeave(evt, element, scope.dropStyle);
                });
                element.bind('dragover', function(evt){
                    dragOver(evt, element, scope.dropStyle);
                });
                element.bind('drop', function(evt){
                    drop(evt, element, scope.dropStyle);
                });
            }
        }
    }]);
