'use strict';

angular.module('valphoneApp')
    .directive('ngFocus', function(){
      return function(scope, elem, attrs){
          elem.bind('focus', function(){
              scope.$apply(attrs.ngFocus);
          });
      }
    }) ;
