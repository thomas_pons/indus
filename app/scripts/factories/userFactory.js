'use strict';

angular.module('valphoneApp')
    .factory('UserFactory', function(){

        var users = [
            {id:1, firstName: "Tom", lastName: "Hanks", phone: "+33601020304", mail: "tom.hanks@valtech.fr", img: "images/tom.jpg", agency: "Valtech Toulouse"},
            {id:2, firstName: "Tim", lastName: "Burton", phone: "+33601020304", mail: "tim.burton@valtech.fr", img: "images/tim.jpg", agency: "Valtech Paris"},
            {id:3, firstName: "Will", lastName: "Smith", phone: "+33601020304", mail: "will.smith@valtech.fr", img: "images/will.jpg", agency: "Valtech Paris"}
        ];

        var agencies = [
            {id:1, name: "Valtech Toulouse", city: "Toulouse", zipCode: "31500", address: "5 avenue Marcel Dassault"},
            {id:2, name: "Valtech Paris", city: "Paris", zipCode: "75007", address: "103 Rue de Grenelle"}
        ];

        return {

            getUsers: function(){
                return users;
            },

            getUser: function(id){
               return users[id - 1];
            },

            addUser: function(user){
                 user.id = users.length + 1;
                 users.push({
                     id: user.id,
                     firstName: user.firstName,
                     lastName: user.lastName,
                     phone: user.phone,
                     mail: user.mail,
                     agency: user.agency
                 })
            },

            getAgencies: function(){
                return agencies;
            },

            getAgency: function(id){
                return agencies[id - 1];
            },

            getAgencyByName: function(name){
                var id;

                for(var i in agencies){
                    var agency = agencies[i];
                    if(agency.name == name){
                        id = agency.id;
                    }

                    return id;
                }
            }
        }
    });
