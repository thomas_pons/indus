'use strict';

angular.module('valphoneApp', [])
  .config(function ($locationProvider, $routeProvider) {


    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/add', {
        templateUrl: 'views/user/add.html',
        controller: 'UserAddCtrl'
      })
      .when('/users', {
        templateUrl: 'views/user/users.html',
        controller: 'UsersCtrl'
      })
      .when('/users/:id', {
        templateUrl: 'views/user/user.html',
        controller: 'UserCtrl'
      })
      .when('/error/404', {
        templateUrl: '404.html'
      })
      .when('/agencies',{
        templateUrl: 'views/agency/agencies.html',
        controller: 'AgenciesCtrl'
       })
      .when('/agency/:id',{
        templateUrl: 'views/agency/agency.html',
        controller: 'AgencyCtrl'
      })
      .otherwise({
        templateUrl: '404.html'
      });

      $locationProvider.html5Mode(true);
      $locationProvider.hashPrefix = '!';
  });
