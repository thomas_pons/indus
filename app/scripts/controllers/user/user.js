'use strict';

angular.module('valphoneApp')
    .controller('UserCtrl', function ($scope, UserFactory, $routeParams, $location) {

        $scope.userDetails = UserFactory.getUser($routeParams.id);
        if(typeof($scope.userDetails) === 'undefined'){
            $location.path('/error/404');
        }

    });
