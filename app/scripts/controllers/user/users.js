'use strict';

angular.module('valphoneApp')
    .controller('UsersCtrl', function ($scope, UserFactory, $routeParams, $location) {

        $scope.users = UserFactory.getUsers();

    });
