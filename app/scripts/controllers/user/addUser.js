'use strict';

angular.module('valphoneApp')
    .controller('UserAddCtrl', function ($scope, UserFactory, $rootScope) {

        $scope.agencies = UserFactory.getAgencies();

        $scope.saveContact = function(){
            UserFactory.addUser($scope.user);
            toastr.success('The user ' + $scope.user.firstName + ' ' + $scope.user.lastName + ' has been added.');
            $scope.user = '';
        }

        $scope.error = function(value, elem){
            if(value == undefined){
                elem.isEmpty = true;
            } else {
                elem.isEmpty = false;
            }
        }
    });
