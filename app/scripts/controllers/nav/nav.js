'use strict';

angular.module('valphoneApp')
    .controller('NavCtrl', function ($scope, $location) {

        $scope.intro = function(){
            if($location.path() != '/'){
                toastr.warning("The help is only available on the home page for the moment.");
            }else{
                introJs().start();
            }
        }
    });