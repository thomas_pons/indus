'use strict';

angular.module('valphoneApp')
    .controller('AgenciesCtrl', function ($scope, UserFactory, $routeParams, $location) {

        $scope.agencies = UserFactory.getAgencies();

    });
