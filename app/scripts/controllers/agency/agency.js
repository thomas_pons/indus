'use strict';

angular.module('valphoneApp')
    .controller('AgencyCtrl', function ($scope, UserFactory, $routeParams, $location) {

        $scope.agencyDetails = UserFactory.getAgency($routeParams.id);

        if(typeof($scope.agencyDetails) === 'undefined'){
            $location.path('/error/404');
        }else{
            var geocoder = new google.maps.Geocoder();
            var address = $scope.agencyDetails.address + ' ' + $scope.agencyDetails.zipCode + ' ' + $scope.agencyDetails.city ;
            var location = null;

            geocoder.geocode({'address': address}, function(results, status){
                if (status == google.maps.GeocoderStatus.OK) {

                    location = results[0].geometry.location;

                    var mapOptions = {
                        zoom: 15,
                        center:  new google.maps.LatLng(location.lat(), location.lng()),
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    };

                    var map = new google.maps.Map(document.getElementById('googleMap'), mapOptions);

                    var marker = new google.maps.Marker({
                        map: map,
                        position: results[0].geometry.location
                    });
                }
            });
        }
    });