'use strict';

describe('Factory: UserFactory', function () {

  // load the factory's module
  beforeEach(module('valphoneApp'));

  // inject the Factory
  it('should have a length of 3 on a getUsers call', inject(function(UserFactory){
    expect(UserFactory.getUsers().length).toEqual(3);
  }));

  it('should add a user to the UserFactory array', inject(function(UserFactory){
    var user = {
      firstName: "TestFirstName",
      lastName: "TestLastName",
      phone: "0102030405",
      mail: "mail@test.com"
    }

    UserFactory.addUser(user);
    expect(UserFactory.getUsers().length).toEqual(4);
  }));
});